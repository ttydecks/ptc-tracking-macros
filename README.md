# PTC tracking Macros
***
This is a set of PTC tracking macros for use within MAD-X.<br>
Call the macro collection in a MAD-X session with

```
CALL, FILE='path/to/trackMacros.madx';
```

A short guide should appear on the screen together with instructions to execute

```
EXEC, helpTM;
```

for further instructions and help.
